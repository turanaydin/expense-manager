/**
 * 
 */
package com.turan.expensemanager.dao;

import com.turan.expensemanager.commons.dao.GenericDao;
import com.turan.expensemanager.domain.ExpenseEntity;


/**
 * @author Turan Ayd�n
 *
 */
public interface ExpenseDao extends GenericDao<ExpenseEntity, Long> {
	/**
	 * Queries database for user name availability
	 * 
	 * @param userName
	 * @return true if available
	 */
	public boolean checkAvailable(int id);
	
	/**
	 * Queries user by username
	 * 
	 * @param userName
	 * @return User entity
	 */
	public ExpenseEntity loadEntityById(int id);
}
