package com.turan.expensemanager.dao;

import com.turan.expensemanager.commons.dao.GenericDao;
import com.turan.expensemanager.domain.UserEntity;

/**
 * Data access object interface to work with User entity database operations.
 * 
 * @author Turan Ayd�n
 */
public interface UserDao extends GenericDao<UserEntity, Long> {

	/**
	 * Queries database for user name availability
	 * 
	 * @param userName
	 * @return true if available
	 */
	boolean checkAvailable(String userName);
	
	/**
	 * Queries user by username
	 * 
	 * @param userName
	 * @return User entity
	 */
	public UserEntity loadUserByUserName(String userName);
	
	
	public UserEntity findByName(String loginName);
}
