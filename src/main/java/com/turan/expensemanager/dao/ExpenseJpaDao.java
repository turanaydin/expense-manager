/**
 * 
 */
package com.turan.expensemanager.dao;

import javax.persistence.NoResultException;
import javax.persistence.Query;

import org.springframework.util.Assert;

import com.turan.expensemanager.commons.dao.GenericJpaDao;
import com.turan.expensemanager.domain.ExpenseEntity;

/**
 * @author Turan
 *
 */
public class ExpenseJpaDao extends GenericJpaDao<ExpenseEntity, Long>  implements ExpenseDao {

	public ExpenseJpaDao() {
		super(ExpenseEntity.class);
	}

	/**
	 * Queries database for user name availability
	 * 
	 * @param userName
	 * @return true if available
	 */
	public boolean checkAvailable(int id) {
		Assert.notNull(id);
		
		Query query = getEntityManager()
			.createQuery("select count(*) from " + getPersistentClass().getSimpleName() 
					+ " u where u.id= :id").setParameter("id", id);
		
		Long count = (Long) query.getSingleResult();
		
		return count < 1;
	}

	/**
	 * Queries user by username
	 * 
	 * @param userName
	 * @return User entity
	 */
	public ExpenseEntity loadEntityById(int id) {
		Assert.notNull(id);
		
		ExpenseEntity expense = null;
		
		Query query = getEntityManager().createQuery("select u from " + getPersistentClass().getSimpleName()
				+ " u where u.id = :id").setParameter("id", id);
		
		try {
			expense = (ExpenseEntity) query.getSingleResult();
		} catch(NoResultException e) {
			//do nothing
		}
		
		return expense;
	}

}
