package com.turan.expensemanager.dao;

import java.util.List;

import javax.persistence.NoResultException;
import javax.persistence.Query;

import org.apache.log4j.Logger;
import org.springframework.util.Assert;

import com.turan.expensemanager.commons.dao.GenericJpaDao;
import com.turan.expensemanager.domain.UserEntity;




/**
 * Data access object JPA impl to work with User entity database operations.
 * 
 * @author Turan Ayd�n
 */

public class UserJpaDao extends GenericJpaDao<UserEntity, Long> implements UserDao {

	final static  Logger logger = Logger.getLogger(UserJpaDao.class);
	public UserJpaDao() {
		super(UserEntity.class);
	}

	/**
	 * Queries database for user name availability
	 * 
	 * @param userName
	 * @return true if available
	 */
	public boolean checkAvailable(String userName) {
		Assert.notNull(userName);
		
		Query query = getEntityManager()
			.createQuery("select count(*) from " + getPersistentClass().getSimpleName() 
					+ " u where u.loginName = :userName").setParameter("userName", userName);
		
		Long count = (Long) query.getSingleResult();
		
		return count < 1;
	}

	/**
	 * Queries user by username
	 * 
	 * @param userName
	 * @return User entity
	 */
	public UserEntity loadUserByUserName(String userName) {
		Assert.notNull(userName);
		
		UserEntity user = null;
		
		Query query = getEntityManager().createQuery("select u from " + getPersistentClass().getSimpleName()
				+ " u where u.loginName = :userName").setParameter("userName", userName);
		
		try {
			user = (UserEntity) query.getSingleResult();
		} catch(NoResultException e) {
			logger.info("User not found! " + user);
		}
		
		return user;
	}


	public UserEntity findByName(String loginName) {
		List<UserEntity> users = findAll();
		for (UserEntity ue : users) {
			if (ue.getLoginName().equals(loginName))
				return ue;
			
		}
		return null;
	}
	

}
