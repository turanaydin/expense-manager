package com.turan.expensemanager.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.springframework.security.authentication.encoding.Md5PasswordEncoder;
import org.springframework.security.authentication.encoding.PasswordEncoder;

import com.turan.expensemanager.commons.domain.BaseEntity;

/**
 * Entity to hold application user data - first name, last name, etc.
 * 
 * @author Turan Ayd�n
 */
@Entity
@Table(name="User")
public class UserEntity extends BaseEntity {



	private static final long serialVersionUID = -8789920463809744548L;

	
	@Column(name = "firstName")
	private String firstName;
	
	@Column(name = "lastName")
	private String lastName;
	
	@Column(name = "loginName")
	private String loginName;
	
	@Column(name = "mail")
	private String mail;
	
	@Column(name = "phone")
	private String phone;
	
	@Column(name = "password")
	private String password;    
	/**
	 * @return the firstName
	 */
	public String getFirstName() {
		return firstName;
	}



	/**
	 * @param firstName the firstName to set
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}



	/**
	 * @return the lastName
	 */
	public String getLastName() {
		return lastName;
	}



	/**
	 * @param lastName the lastName to set
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}



	/**
	 * @return the loginName
	 */
	public String getLoginName() {
		return loginName;
	}



	/**
	 * @param loginName the loginName to set
	 */
	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}



	/**
	 * @return the mail
	 */
	public String getMail() {
		return mail;
	}



	/**
	 * @param mail the mail to set
	 */
	public void setMail(String mail) {
		this.mail = mail;
	}



	/**
	 * @return the userPhone
	 */
	public String getPhone() {
		return phone;
	}



	/**
	 * @param userPhone the userPhone to set
	 */
	public void setPhone(String phone) {
		this.phone = phone;
	}



	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}

	
	public void setPassword(String password) {
		PasswordEncoder crypto = new Md5PasswordEncoder();
		this.password = crypto.encodePassword(password, null);
	}

	

	@Override
	public String toString() {
		return "UserEntity : " + getId() + ", " + firstName + ", " + lastName + ", " + loginName + ", " + mail + ", " + phone +", "+ password;
	}
	
	
}
