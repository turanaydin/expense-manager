/**
 * 
 */
package com.turan.expensemanager.domain;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import org.apache.log4j.Logger;
import com.turan.expensemanager.commons.domain.BaseEntity;

/**
 * @author Turan
 *
 */
@Entity
@Table(name="ExpenseT")
public class ExpenseEntity extends BaseEntity {

	private static final long serialVersionUID = -2977952458569023889L;
	
	final static  Logger logger = Logger.getLogger(ExpenseEntity.class);
	
	@Column(name="user")
	private String userName;
	
	@Column(name="date")
	private Date date;
	
	@Column(name="amount")
	private double amount;
	
	@Column(name="comment")
	private String comment;

	/**
	 * @return the userName
	 */
	public String getUserName() {
		return userName;
	}

	/**
	 * @param userName the userName to set
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}

	/**
	 * @return the date
	 */
	public Date getDate() {
		return date;
	}

	/**
	 * @param date the date to set
	 */
	public void setDate(Date date) {
		this.date = date;
	}

	/**
	 * @return the amount
	 */
	public double getAmount() {
		return amount;
	}

	/**
	 * @param amount the amount to set
	 */
	public void setAmount(double amount) {
		this.amount = amount;
	}

	/**
	 * @return the comment
	 */
	public String getComment() {
		return comment;
	}

	/**
	 * @param comment the comment to set
	 */
	public void setComment(String comment) {
		this.comment = comment;
	}
	
	

}
