/**
 * 
 */
package com.turan.expensemanager.services;
import com.turan.expensemanager.domain.ExpenseEntity;


/**
 * @author Turan
 *
 */
public interface ExpenseService {
	/**
	 * Create user - persist to database
	 * 
	 * @param userEntity
	 * @return true if success
	 */
	boolean createExpense(ExpenseEntity expenseEntity);
	
}
