package com.turan.expensemanager.services;

import com.turan.expensemanager.domain.UserEntity;

/**
 * Provides processing service to set user authentication session
 * 
 * @author Turan Ayd�n
 */
public interface UserAuthenticationProviderService {

	/**
	 * Process user authentication
	 * 
	 * @param user
	 * @return
	 */
	boolean processUserAuthentication(UserEntity user);
}
