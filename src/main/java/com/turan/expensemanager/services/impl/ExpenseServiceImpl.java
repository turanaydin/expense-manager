/**
 * 
 */
package com.turan.expensemanager.services.impl;


import org.apache.log4j.Logger;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;

import com.turan.expensemanager.dao.ExpenseDao;
import com.turan.expensemanager.domain.ExpenseEntity;
import com.turan.expensemanager.services.ExpenseService;


/**
 * @author Turan
 *
 */
public class ExpenseServiceImpl implements ExpenseService {

	private ExpenseDao expenseDao;
	final static  Logger logger = Logger.getLogger(ExpenseServiceImpl.class);
	
	/**
	 * Create  - persist to database
	 * 
	 * @param 
	 * @return true if success
	 */
	public boolean createExpense(ExpenseEntity expenseEntity) {
		
		expenseEntity.setUserName(getCurrentUserName());
		expenseDao.save(expenseEntity);
		return true;
	
	}
	public String getCurrentUserName()
	{
		Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		String username;
		if (principal instanceof UserDetails) {
			username = ((UserDetails)principal).getUsername();
		} else {
			username = principal.toString();
		}
		return username;
	}
	
	
	public ExpenseDao getExpenseDao() {
		return expenseDao;
	}
	public void setExpenseDao(ExpenseDao expenseDao) {
		this.expenseDao = expenseDao;
	}
}
